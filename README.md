![start2](https://cloud.githubusercontent.com/assets/10303538/6315586/9463fa5c-ba06-11e4-8f30-ce7d8219c27d.png)

# Broadcaster

用于PocketMine-MP的高级广播插件。

## 种类

PocketMine-MP插件

## 要求

PocketMine-MP 1.7dev API 3.0.0-ALPHA7 -> 3.0.0-ALPHA11

## 概述

**Broadcaster** 是一个高级的PocketMine-MP广播插件。<br>
通过广播，您可以设置自定义消息，弹出和标题广播。你也可以用/sm，弹出/sp和标题的/st命令。<br>
这个插件还让你定制颜色 (你可以使用 & 标记取代 § 在文本中)，前缀，后缀和间隔。

**EvolSoft网站：** https://www.evolsoft.tk

***这个插件使用新的API。你不能把它安装在旧版本的PocketMine上。***

## 文档

**配置 (config.yml):**

```yaml
---
# Available tags for broadcast messages, popups and titles:
# - {MAXPLAYERS}: 显示服务器支持的最大玩家数。
# - {TOTALPLAYERS}: 显示所有在线玩家的数量。
# - {PREFIX}: 显示前缀
# - {SUFFIX}: 显示前缀
# - {TIME}: 显示当前时间
# Available tags for /sendmessage, /sendpopup and /sendtitle format:
# - {MESSAGE}: 提示消息
# - {MAXPLAYERS}: 显示服务器支持的最大玩家数。
# - {TOTALPLAYERS}: 显示所有在线玩家的数量。
# - {PREFIX}: 提示消息
# - {PLAYER}: 消息接收方
# - {SENDER}: 显示发送者的名字
# - {SUFFIX}: 提示消息
# - {TIME}: 显示当前时间
# Extra tag for titles:
# - {SUBTITLE}: 添加字幕 (此标记后的文本将是副标题的内容。) 
# Prefix
prefix: "&9[&eBroadcaster&9]"
# Suffix
suffix: "[A]"
# Date\Time format (replaced in {TIME}). For format codes read http://php.net/manual/en/datetime.formats.php
datetime-format: "H:i:s"
# Message broadcast
message-broadcast:
 # Enable message broadcast
 enabled: true
 # Broadcast interval (in seconds)
 time: 15
 # Command /sendmessage format
 command-format: "&e[{TIME}] {PREFIX} {SUFFIX} &a{SENDER}&e>&f {MESSAGE}"
 # Broadcast messages
 messages:
  - "&e[{TIME}] {PREFIX}&f 1st message"
  - "&e[{TIME}] {PREFIX}&f 2nd message"
  - "&e[{TIME}] {PREFIX}&f 3rd message"
# Popup broadcast
popup-broadcast:
 # Enable popup broadcast
 enabled: true
 # Popup broadcast interval (in seconds)
 time: 15
 # Popup duration (in seconds)
 duration: 5
 # Command /sendpopup format
 command-format: "&a{SENDER}&e>>&f {MESSAGE}"
 # Popup broadcast messages
 messages:
  - "&aWelcome to your server"
  - "&d{TOTALPLAYERS} &eof &d{MAXPLAYERS} &eonline"
  - "&bCurrent Time: &a{TIME}"
# Title broadcast
title-broadcast:
 # Enable title broadcast
 enabled: true
 # Title broadcast interval
 time: 30
 # Command /sendtitle format
 command-format: "&d{MESSAGE}"
 # Title broadcast messages
 messages:
  - "&aWelcome to your server!{SUBTITLE}&bGood game!"
  - "&eHello player!"
...
```

**指令:**

<dd><b><i>/broadcaster</b> - Broadcaster commands (aliases: [bc, broadcast])</i></dd><br>
<dd><i><b>/sendmessage &lt;player (* for all players)&gt; &lt;message&gt;</b> - Send message to player(s) (aliases: [sm, smsg])</i></dd><br>
<dd><i><b>/sendpopup &lt;player (* for all players)&gt; &lt;message&gt;</b> - Send popup to player(s) (aliases: [sp, spop])</i></dd><br>
<dd><i><b>/sendtitle &lt;player (* for all players)&gt; &lt;message&gt;</b> - Send title to player(s) (aliases: [st, stl])</i></dd><br>

**权限**

- <dd><i><b>broadcaster.*</b> - Broadcaster permissions tree.</i>
- <dd><i><b>broadcaster.info</b> - Let player read info about Broadcaster.</i>
- <dd><i><b>broadcaster.reload</b> - Let player reload Broadcaster.</i>
- <dd><i><b>broadcaster.sendmessage</b> - Let player send messages to players with /sendmessage command.</i>
- <dd><i><b>broadcaster.sendpopup</b> - Let player send popups to players with /sendpopup command.</i>
- <dd><i><b>broadcaster.sendpopup</b> - Let player send titles to players with /sendtitle command.</i>

## API

几乎所有的插件都有API访问来广泛扩展它们的特性。

To access Broadcaster API:<br>
*1. 定义plugin.yml中的插件依赖项。 (you can check if Broadcaster is installed in different ways):*

```yaml
depend: [Broadcaster]
```

*2. 在你的插件代码中包含广播Broadcaster API：*

```php
//Broadcaster API
use Broadcaster\Broadcaster;
```

*3. 通过以下方式访问API:*

```php
Broadcaster::getAPI()
```
